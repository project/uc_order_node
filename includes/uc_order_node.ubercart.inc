<?php

/**
 * @file
 * Ubercart order node ubercart module integration.
 */

/*************************************************************************
 * Ubercart hooks                                                         *
 * ***********************************************************************/

/**
 * Implementation of hook_order().
 */
function uc_order_node_order($op, &$arg1, $arg2) {
  switch ($op) {
    case 'load':
      // Add the associated node object to the order object.
      $order_node = uc_order_node_load($arg1->order_id);
      if ($order_node) {
        $arg1->node = $order_node;
      }
      break;
    case 'save':

      // Add the associated node object to the order object if it isn't already, so that other modules can use it during order saving.
      if (empty($arg1->node)) {

        // Try to load an existing order node.
        $order_node = uc_order_node_load($arg1->order_id);
        if ($order_node) {
          $arg1->node = $order_node;
        }

        // If an existing order node wasn't found...
        else {
          
          // Create an order node if this order's status is "in_checkout", and the checkout setting is TRUE.
          if ($arg1->order_status == 'in_checkout' && variable_get('uc_order_node_checkout', 1)) {

            // Create the order node. Disable required fields so that validation doesn't fail.
            $arg1->node = uc_order_node_create($arg1->order_id, NULL, FALSE);
          }
        }
      }
      break;
    case 'delete':
      // Delete the order node.
      $order_node = uc_order_node_load($arg1->order_id);
      if ($order_node) {
        node_delete($order_node->nid);
      }
      break;
  }
}

/**
 * Implementation of hook_order_pane().
 */
function uc_order_node_order_pane() {
  // Register pane for artwork image field.
  $panes[] = array(
    'id' => 'order_node',
    'callback' => 'uc_order_node_order_pane_callback',
    'title' => t('Order Information'),
    'desc' => t(''),
    'class' => 'pos-left',
    'weight' => 0,
    'show' => array('view', 'edit'),
  );
  return $panes;
}

/*************************************************************************
 * Ubercart callbacks                                                     *
 * ***********************************************************************/

/**
 * Order pane callback.
 */
function uc_order_node_order_pane_callback($op, $arg1) {
  $order = $arg1;  // This will be useful in the 'view' and 'edit-form'.
  switch ($op) {
    case 'view':
      if (!empty($arg1->node)) {  // First make sure a node is available for the order.
        return uc_order_node_view($order->node);  // Display the node view.
      }
      else {
        break;
      }
    case 'edit-form':
      $form_state = array();
      $form['uc_order_node'] = uc_order_node_edit_form($form_state, $order->order_id);
      return $form;
    case 'edit-theme':
      if (!empty($arg1['uc_order_node'])) {
        return drupal_render($arg1['uc_order_node']);
      }
      else {
        break; 
      }
    case 'edit-process':
      uc_order_node_edit_form_process($arg1);
      break;
  }
}