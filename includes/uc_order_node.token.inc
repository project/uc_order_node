<?php

/**
 * @file
 * Ubercart order node token module integration.
 */

/*************************************************************************
 * Token hooks                                                            *
 * ***********************************************************************/

/**
 * Implementation of hook_token_values().
 */
function uc_order_node_token_values($type, $object = NULL, $options = array()) {
  $tokens = array();
  if ($type == 'order' && !empty($object->node)) {
    // We're basically just hijacking CCK's tokens, but enabling them in the order context, and passing in the order node.
    $tokens = content_token_values('node', $object->node);
  }
  return $tokens;
}

/**
 * Implementation of hook_token_list().
 */
function uc_order_node_token_list($type = 'all') {
  $uc_order_node_type = variable_get('uc_order_node_type', 0);
  $tokens = array();
  if ($uc_order_node_type) {
    if ($type == $uc_order_node_type) {
      // Hijack CCK's tokens
      module_load_include('inc', 'content', 'includes/content.token');
      $tokens = content_token_list('node');
    }
  }
  return $tokens;
}