<?php

/**
 * @file
 * Ubercart order node views integration.
 */

/**
 * Implementation of hook_views_data().
 */
function uc_order_node_views_data() {
  $data['uc_order_node']['table']['group'] = t('UC Order Node');
  $data['uc_order_node']['table']['join']= array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'node_revisions' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'uc_orders' => array(
      'left_field' => 'order_id',
      'field' => 'oid',
    ),
  );
  $data['uc_order_node']['nid'] = array(
    'title' => t('Order Node ID'),
    'help' => '(no help available)',
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
    ),
  );
  $data['uc_order_node']['oid'] = array(
    'title' => t('Order ID'),
    'help' => '(no help available)',
    'relationship' => array(
      'base' => 'uc_orders',
      'field' => 'order_id',
      'handler' => 'views_handler_relationship',
    ),
  );
  return $data;
}