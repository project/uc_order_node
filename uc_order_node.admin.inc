<?php

/**
 * @file
 * Ubercart order node admin interface.
 */

/**
 * Ubercart order node settings form callback.
 */
function uc_order_node_settings_form(&$form_state) {

  // Set the page title
  drupal_set_title('Order Node settings');

  // Load the node types
  $types = node_get_types();

  // Assemble the options array
  $options = array('0' => '(none)');
  if (count($types) > 0) {
    foreach ($types as $name => $type) {
      $options[$name] = $type->name;
    }
  }

  // Build the node select list
  $form['uc_order_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Order Node Type'),
    '#description' => t('Which nodetype would you like to associate with Ubercart Orders? When new orders are created, a node of this type will also be created and associated with the order. If the Order is deleted, so is the node. The node can be viewed and edited from the order\'s View and Edit tabs, respectively. <strong>Warning: Changing this value after orders have been created could result in confusing stuff happening.</strong>'),
    '#options' => $options,
    '#default_value' => variable_get('uc_order_node_type', 0),  // Check for a default value
  );

  // Allow the user to define who the author of order nodes should be. Default to the user that created the node.
  $form['uc_order_node_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Node author'),
    '#description' => t('Who should the author of the order nodes be when they are created? Leave this field blank to assign it to the user that created the order in Ubercart.'),
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => variable_get('uc_order_node_username', ''),
  );

  // Allow the user to turn off order node creation during checkout.
  $form['uc_order_node_checkout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create order node during checkout'),
    '#description' => t('Should an order node be created during the checkout process?'),
    '#default_value' => variable_get('uc_order_node_checkout', 1),
  );

  // Pass the form through the system_settings_form() function and return it
  return system_settings_form($form);
}

/**
 * Ubercart order node settings form validation.
 */
function uc_order_node_settings_form_validate(&$form, &$form_state) {

  // Make sure the username exists
  $name = $form_state['values']['uc_order_node_username'];
  if (!db_result(db_query("SELECT COUNT(*) FROM {users} WHERE name = '%s';", $name))) {
    form_set_error('uc_order_node_username', 'The username "' . $name . '" doesn\'t exist.');
  }
}